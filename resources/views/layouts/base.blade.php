@push('css1')
<link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/elegant_font/style.css') }}">
@endpush
@push('css2')
<link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/slider-pro.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/owl.theme.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/owl.transitions.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/elegant_font/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/modernizr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.easing.1.3.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.scrollUp.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.easypiechart.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/isotope.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.fitvids.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.stellar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.nav.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/imagesloaded.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/smooth-scroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.sliderPro.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
@endpush

@push('ie7script')
<script type="text/javascript" src="{{ asset('assets/elegant_font/lte-ie7.js') }}"></script>
@endpush

@push('ie9script')
<script type="text/javascript" src="{{ asset('assets/js/html5shiv.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/respond.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/selectivizr.js') }}"></script>
@endpush
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="Author" content="">
    <title>Nestiwnds.com</title>
    @stack('css1')
    <!--[if lte IE 7]>
    @stack('ie7script')
    <![endif]-->
    @stack('css2')


    <!--[if lt IE 9]>
    @stack('ie9script')
    <![endif]-->
</head>
<body>
<!-- Header Start -->
<header>
    <!-- Navigation Menu start-->
    <nav id="topNav" class="navbar navbar-default main-menu">
        <div class="container">
            <button class="navbar-toggler hidden-md-up pull-right" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
                ☰
            </button>
            <a class="navbar-brand page-scroll" href="/" style="background-image: url(images/nw_logo_white.png);background-repeat:no-repeat; background-size:100% 100%;-moz-background-size:100% 100%;color:transparent;">Nestwinds.com
            </a>
            <div class="collapse navbar-toggleable-sm" id="collapsingNavbar">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#slider">HOME</a>
                    </li>
                    <li>
                        <a href="#services">SERVICES</a>
                    </li>
                    <li>
                        <a href="#about">ABOUT</a>
                    </li>
                    <li>
                        <a href="#portfolio">Best works!</a>
                    </li>
                    <li>
                        <a href="#team">TEAM</a>
                    </li>
                    <li>
                        <a href="#clients">CLIENTS</a>
                    </li>
                    <li>
                        <a href="#contact">CONTACT US</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


</header>
<!-- Header End -->
<section class="slider-pro slider" id="slider">
    <div class="sp-slides">
        <!-- Slides -->
        <div class="sp-slide main-slides">
            <div class="img-overlay"></div>
            <img class="sp-image" src="images/slider/slider-img-1.jpg" alt="Slider 1"/>
            <h1 class="sp-layer slider-text-big"
                data-position="center" data-show-transition="left" data-hide-transition="right" data-show-delay="1500" data-hide-delay="200">
                <span class="highlight-texts">Welcome to Nestwinds!</span>
            </h1>
            <p class="sp-layer"
               data-position="center" data-vertical="15%" data-show-delay="2000" data-hide-delay="200" data-show-transition="left" data-hide-transition="right">
                best business
            </p>
        </div>
        <!-- Slides End -->
        <!-- Slides -->
        <div class="sp-slide main-slides">
            <div class="img-overlay"></div>
            <img class="sp-image" src="images/slider/slider-img-2.jpg" alt="Slider 2"/>

            <h1 class="sp-layer slider-text-big"
                data-position="center" data-show-transition="left" data-hide-transition="right" data-show-delay="1500" data-hide-delay="200">
                <span class="highlight-texts">Executive</span>
            </h1>
            <p class="sp-layer"
               data-position="center" data-vertical="15%" data-show-delay="2000" data-hide-delay="200" data-show-transition="left" data-hide-transition="right">
                best business
            </p>
        </div>
        <!-- Slides End -->
        <!-- Slides -->
        <div class="sp-slide main-slides">
            <div class="img-overlay"></div>
            <img class="sp-image" src="images/slider/slider-img-3.jpg" alt="Slider 3"/>
            <h1 class="sp-layer slider-text-big"
                data-position="center" data-show-transition="left" data-hide-transition="right" data-show-delay="1500" data-hide-delay="200">
                <span class="highlight-texts">Nestwinds</span>
            </h1>
            <p class="sp-layer"
               data-position="center" data-vertical="15%" data-show-delay="2000" data-hide-delay="200" data-show-transition="left" data-hide-transition="right">
                best business
            </p>
        </div>
        <!-- Slides End -->
    </div>
</section>
<!-- Main Slider End -->
<div id="content12" data-section="content-12" class="data-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 nopaddingnomargin">
                <div class="content-center">
                    <h3>WELCOME to Nestwinds</h3>
                    <p>The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                    <p>The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                    <p>The generated Lorem Ipsum is therefore always free from repetition.</p>
                </div>
            </div>
            <div class="col-md-6 nopaddingnomargin">
                <div class="content-right">
                    <h3>Looking for best consultancy for your business, Here is the more details of our amazing services <br/> <b>BEST in INDUSTRY</b></h3>
                    <a href="#" class="btn btn-default-blue">Read More</a>
                </div>
            </div>
        </div>
    </div>
</div> <!-- /#content12 -->
<section id="services" class="section-wrapper">
    <div class="container">
        <div class="row">
            <!-- Section Header -->
            <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                <h2><span class="highlight-text">Services</span></h2>
                <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, nam corporis quas, saepe minima error aperiam dolorum aliquam, quis deserunt eos eius quisquam odio itaque.</p>
            </div>
            <!-- Section Header End -->

            <div class="our-services">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".2s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_piechart"></i> <h3>Graphic Design</h3>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero praesentium quam eos eius quisquam odio itaque.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".2s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_desktop"></i><h3>Webdesign </h3>
                            </div>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero praesentium quam eos eius quisquam odio itaque.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".2s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_currency"></i><h3>Mobiel Dev</h3>
                            </div>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero praesentium quam eos eius quisquam odio itaque.</p>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_laptop"></i><h3>App Dev</h3>
                            </div>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero praesentium quam eos eius quisquam odio itaque.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_image"></i><h3>Outsourcing</h3>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero praesentium quam eos eius quisquam odio itaque.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_volume-high"></i><h3>Quality</h3>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero praesentium quam eos eius quisquam odio itaque.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="about" class="about-sec section-wrapper description">
    <div class="container">
        <div class="row">
            <!-- Section Header -->
            <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                <h2><span class="highlight-text">About</span></h2>

                <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, nam corporis quas, saepe minima error aperiam dolorum aliquam, quis deserunt eos eius quisquam odio itaque.</p>
            </div>
            <!-- Section Header End -->

            <div class="col-md-6 col-sm-6 col-xs-12 custom-sec-img wow fadeInDown">
                <img src="images/features.png" alt="Custom Image">
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 customized-text wow fadeInDown black-ed">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa sit, numquam amet voluptatibus obcaecati ea maiores totam nostrum, ad iure rerum quas harum ipsum.  lobcaecati ea maiores totam nostrum, ad iure rerum quas harum ipsum. Rem ea ducimus quos quae quo.</p>
                <div class="row">
                    <div class="col-md-11">
                        <strong>Bootstrap</strong>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam iusto, natus est ducimus saepe laborum</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <strong>Responisve Theme</strong>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam iusto, natus est ducimus saepe laborum Lorem ipsum dolor sit amet.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <strong>HTML5/CSS3</strong>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam iusto, natus est ducimus saepe laborum Lorem ipsum dolor sit amet.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="portfolio" class="bgSection portfolio-section">
    <div class="container">
        <div class="row">

            <!-- Section Header -->
            <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                <h2><span class="highlight-text">Best works!</span></h2>

                <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, nam corporis quas, saepe minima error aperiam dolorum aliquam, quis deserunt eos eius quisquam odio itaque.</p>
            </div>
            <!-- Section Header End -->

        </div>
    </div>

    <!-- Works -->
    <div class="portfolio-works wow fadeIn" data-wow-duration="2s">
        <!-- Filter Button Start -->
        <div id="portfolio-filter" class="portfolio-filter-btn-group">
            <ul>
                <li>
                    <a href="#" class="selected" data-filter="*">All</a>
                    <a href="#" data-filter=".web">Web</a>
                    <a href="#" data-filter=".app">App</a>
                </li>
            </ul>
        </div>
        <!-- Filter Button End -->
        <div class="portfolio-items">
            <!-- Portfolio Items -->
            <div class="item portfolio-item web app">
                <img src="images/img-portfolio/aport.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="images/img-portfolio/aport.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Portfolio Items End -->
            <!-- Portfolio Items -->
            <div class="item portfolio-item app">
                <img src="images/img-portfolio/genpara.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="images/img-portfolio/genpara.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Portfolio Items End -->
            <!-- Portfolio Items -->
            <div class="item portfolio-item app">
                <img src="images/img-portfolio/ilmio.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="images/img-portfolio/ilmio.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Portfolio Items End -->
            <!-- Portfolio Items -->
            <div class="item portfolio-item web app">
                <img src="images/img-portfolio/salient.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="images/img-portfolio/salient.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Portfolio Items -->
            <!-- Portfolio Items -->
            <div class="item portfolio-item app">
                <img src="images/img-portfolio/ssg.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="images/img-portfolio/ssg.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Portfolio Items End -->
            <!-- Portfolio Items -->
            <div class="item portfolio-item web app">
                <img src="images/img-portfolio/syszo.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="images/img-portfolio/syszo.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Portfolio Items End -->
            <div class="item portfolio-item app">
                <img src="images/img-portfolio/thanks.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="images/img-portfolio/thanks.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Portfolio Items End -->
            <!-- Portfolio Items -->
            <div class="item portfolio-item web">
                <img src="images/img-portfolio/nw.jpg" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="images/img-portfolio/nw.jpg" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Portfolio Items End -->
        </div>
    </div>
    <!-- Works End -->
</section>
<!-- Portfolio Section End -->
<section id="info" class="info-section">
    <div class="container text-xs-center">
        <!-- Section Header -->
        <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
            <h2><span class="highlight-text">Stats</span></h2>

            <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, nam corporis quas, saepe minima error aperiam dolorum aliquam, quis deserunt eos eius quisquam odio itaque.</p>
        </div>
        <!-- Section Header End -->
        <div class="row wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
            <div class="col-md-3 col-sm-6 col-xs-12 text-xs-center">

                <i class="icon_mic_alt wow pulse" style="visibility: visible; animation-name: pulse;"></i>
                <h4>Margins</h4>
                <h1 class="text-primary">100,000</h1>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-xs-center">

                <i class="icon_gift_alt wow pulse" style="visibility: visible; animation-name: pulse;"></i>
                <h4>Completed</h4>
                <h1 class="text-primary">34201</h1>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-xs-center">

                <i class="icon_mobile wow pulse" style="visibility: visible; animation-name: pulse;"></i>
                <h4>Projects</h4>
                <h1 class="text-primary">152</h1>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-xs-center">

                <i class="icon_lightbulb_alt wow pulse" style="visibility: visible; animation-name: pulse;"></i>
                <h4>Customers</h4>
                <h1 class="text-primary">56500</h1>
            </div>
        </div>
    </div>
</section>
<section id="team" class="bgSection teams-section">
    <div class="parallax-overlay"></div>
    <div class="teams-wrapper wow fadeInDown">
        <div class="container">
            <div class="row">

                <!-- Section Header -->
                <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                    <h2><span class="highlight-text-inverted">Team</span></h2>

                    <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, nam corporis quas, saepe minima error aperiam dolorum aliquam, quis deserunt eos eius quisquam odio itaque.</p>
                </div>

                <!-- teams Slider -->
                <div id="teams" class="owl-carousel teams">

                    <!-- Slides -->
                    <div class="teams-slides col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">
                        <img src="images/img-teams/team1.jpg" alt="">
                        <p class="client-info">Chris Willam</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisic</p>

                    </div>
                    <!-- Slides End -->

                    <!-- Slides -->
                    <div class="teams-slides col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">
                        <img src="images/img-teams/team2.jpg" alt="">
                        <p class="client-info">Randy Dode</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisic</p>

                    </div>
                    <!-- Slides End -->

                    <!-- Slides -->
                    <div class="teams-slides col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">
                        <img src="images/img-teams/team3.jpg" alt="">
                        <p class="client-info">Michel Kimte</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisic</p>

                    </div>
                    <!-- Slides End -->

                </div>
            </div>
        </div>
    </div>
</section>


<!-- Clients Section -->
<section id="clients" class="clients-section">
    <!-- Container Ends -->
    <div class="container">
        <div class="row">
            <!-- Section Header -->
            <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                <h2><span class="highlight-text">Clients</span></h2>

                <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, nam corporis quas, saepe minima error aperiam dolorum aliquam, quis deserunt eos eius quisquam odio itaque.</p>
            </div>
            <!-- Section Header End -->
            <div class="wow fadeInUpQuick" data-wow-delay=".9s">
                <div class="row" id="clients-carousel">
                    <div class="client-item-wrapper">
                        <img src="images/clients/A-port.png" alt="">
                    </div>
                    <div class="client-item-wrapper">
                        <img src="images/clients/agNedia.png" alt="">
                    </div>
                    <div class="client-item-wrapper">
                        <img src="images/clients/NaviPlus.png" alt="">
                    </div>
                    <div class="client-item-wrapper">
                        <img src="images/clients/Aruto.png" alt="">
                    </div>
                    <div class="client-item-wrapper">
                        <img src="images/clients/Questar.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Container Ends -->
</section>
<!-- Client Section End -->

<section id="contact" class="section-wrapper contact-section" data-stellar-background-ratio="0.5">
    <div class="parallax-overlay"></div>
    <div class="container">
        <div class="row">

            <!-- Section Header -->
            <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                <h2><span class="highlight-text">Contact Us</span></h2>

                <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">We love feedback. Fill out the form below and we'll get back to you as soon as possible. in minus distinctio dolores ipsam.</p>
            </div>
            <!-- Section Header End -->

            <div class="contact-details">


                <!-- Contact Form -->
                <div class="contact-form wow bounceInRight">

                    <!--NOTE: Update your email Id in "contact_me.php" file in order to receive emails from your contact form-->
                    <form name="sentMessage" id="contactForm"  novalidate>
                        <div class="col-md-6">
                            <input type="text" class="form-control"
                                   placeholder="Full Name" id="name" required
                                   data-validation-required-message="Please enter your name" />
                            <p class="help-block"></p>
                        </div>
                        <div class="col-md-6">
                            <input type="email" class="form-control" placeholder="Email"
                                   id="email" required
                                   data-validation-required-message="Please enter your email" />
                        </div>

                        <div class="col-md-12">
	<textarea rows="10" cols="100" class="form-control"
              placeholder="Message" id="message" required
              data-validation-required-message="Please enter your message" minlength="5"
              data-validation-minlength-message="Min 5 characters"
              maxlength="999" style="resize:none"></textarea>
                        </div>

                        <div class="col-md-4 col-md-offset-5"><br><div id="success"> </div><button type="submit" class="btn btn-primary">Submit Message</button></div>
                    </form>
                </div>

            </div>
        </div>
</section>

<!-- Contact Section End -->
<section class="footer-container">
    <div class="container">
        <div class="row footer-containertent">
            <div class="col-md-4">
                <h4>EXECUTIVE</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et porro quos iste ratione doloribus asperiores, error omnis delectus rerum sapiente. Et, aliquam modi beatae quae in perferendis ab est fugiat!</p>
            </div>
            <div class="col-md-4">
                <h4>News & Updates</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, nam corporis quas, saepe minima error aperiam dolorum aliquam, quis deserunt eos eius quisquam odio itaque.</p>
            </div>
            <div class="col-md-4 contac-us">
                <h4>Contact Us</h4>
                <p>Lorem ipsum dolor sit amet adipisicing elit.</p>
                <ul>
                    <li><i class="fa fa-home"></i>123 New Venu Street</li>
                    <li><i class="fa fa-phone"></i>001 123 12345 99</li>
                    <li><i class="fa fa-envelope-o"></i>support@website.com</li>
                </ul>
            </div>
        </div>
    </div>
</section>


<footer>

    <div class="container">
        <div class="row">
            <div class="footer-containertent">

                <ul class="footer-social-info">
                    <li>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-pinterest"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </li>
                </ul>
                <br/><br/>
                <p>Copyright &copy; 2017.Company name All rights reserved.<a target="_blank" href="http://nestwinds.com">Nestwinds</a></p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->
@stack('scripts')
</body>
</html>